//
//  KazAutocomplateLabel.swift
//  KazakhKeyboardKitTestKeyboard
//
//  Created by Мадияр on 4/18/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit
import KeyboardKit

protocol KazAutocomplateLabelDelegate: class {
    func updateSuggetion()
}

class KazAutocomplateLabel: UILabel {
    
    weak var delegate: KazAutocomplateLabelDelegate?

    convenience init(word: String, proxy: UITextDocumentProxy, vc: UIInputViewController) {
        self.init(frame: .zero)
        self.proxy = proxy
        text = word
        textAlignment = .center
        accessibilityTraits = .button
        textColor = textColor(for: proxy)
        addTapAction { [weak self] in
            let text = word + " "
            self?.proxy?.replaceCurrentWord(with: text)
            self?.delegate?.updateSuggetion()
            
            var arr = userDefaults.getAllWords()
            
            for item in (0...arr.count - 1) {
                if arr[item].word == word {
                    arr[item].count += 1
                }
            }
            
            userDefaults.setAllWords(value: arr)
        }
        delegate = vc as? KazAutocomplateLabelDelegate
    }

    private weak var proxy: UITextDocumentProxy?
}

private extension KazAutocomplateLabel {

    func textColor(for proxy: UITextDocumentProxy) -> UIColor {
        let asset = proxy.keyboardAppearance == .dark
            ? Asset.Colors.darkButtonText
            : Asset.Colors.lightButtonText
        return asset.color
    }
}
