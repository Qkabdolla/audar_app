//
//  Extentions.swift
//  AudarCustomKeyboard
//
//  Created by Мадияр on 5/11/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

let userDefaults = UserDefaults.standard
extension UserDefaults {
    
    enum UserDefaultKeys: String {
        case allWords
    }
    
    func setAllWords(value: [Word]) {
        set(try? PropertyListEncoder().encode(value), forKey: UserDefaultKeys.allWords.rawValue)
    }
    
    func getAllWords() -> [Word] {
        if let data = value(forKey: UserDefaultKeys.allWords.rawValue) as? Data {
            return try! PropertyListDecoder().decode(Array<Word>.self, from: data)
        } else {
            return [Word]()
        }
    }
    
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
