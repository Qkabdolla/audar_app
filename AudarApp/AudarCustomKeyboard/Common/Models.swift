//
//  Model.swift
//  AudarCustomKeyboard
//
//  Created by Мадияр on 5/11/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

struct Word: Codable {
    let word: String
    var count: Int = 0
}

enum AlphabeticKeyboardType {
    case latin
    case cyrillic
}
