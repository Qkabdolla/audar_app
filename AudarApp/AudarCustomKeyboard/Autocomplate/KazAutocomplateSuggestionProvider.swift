//
//  KazAutocomplateSuggestionProvider.swift
//  KazakhKeyboardKitTestKeyboard
//
//  Created by Мадияр on 4/18/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import KeyboardKit

class KazAutocomplateSuggestionProvider: AutocompleteSuggestionProvider {
    
    func autocompleteSuggestions(for text: String, completion: AutocompleteResponse) {
        
        guard text.count > 0 else { return completion(.success([])) }
        let suffixes = findMatchingWords(for: text)
        let suggestions = suffixes.map { $0 }
        completion(.success(suggestions))
    }
    
    @available(*, deprecated, renamed: "autocompleteSuggestions(for:completion:)")
    func provideAutocompleteSuggestions(for text: String, completion: AutocompleteResponse) {
        autocompleteSuggestions(for: text, completion: completion)
    }
    
    // MARK: - Helpers
    
    private func takeWords() -> [Word] {
        var allWords = [Word]()
        
        if let startsWordsPath = Bundle.main.url(forResource: "words", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startsWordsPath, encoding: .utf8) {
                let arr = startWords.components(separatedBy: .newlines)
                arr.forEach { allWords.append(Word(word: $0)) }
            }
        } else {
            allWords = [Word]()
        }
        
        return allWords
    }
    
    private func takeThreeFirst(_ matchedWords: [Word]) -> [String] {
        var arr = [String]()
        
        let sorted = matchedWords.sorted {
            $0.count > $1.count
        }
        
        if sorted.count >= 1 {
            arr.append(sorted[0].word)
            
            if sorted.count >= 2 {
                arr.append(sorted[1].word)
                
                if sorted.count >= 3 {
                    arr.append(sorted[2].word)
                }
            }
        }
        
        return arr
    }
    
    private func findMatchingWords(for text: String) -> [String] {
        var allWords = [Word]()
        var matchedWords = [Word]()
        
        if userDefaults.getAllWords().count == 0 {
            allWords = takeWords()
            userDefaults.setAllWords(value: allWords)
        } else {
            allWords = userDefaults.getAllWords()
        }
        
        allWords.sort { $0.word < $1.word }
        
        allWords.forEach { (str) in
            if str.word.hasPrefix(text) {
                if !(str.word == text) {
                    matchedWords.append(str)
                }
            }
        }
        
        return takeThreeFirst(matchedWords)
    }
}
