//
//  AlphabetCard.swift
//  AudarApp
//
//  Created by rau4o on 4/6/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

struct AlphabetCard {
    var letter = ""
    dynamic var isFlipped: Bool = false
    var imageName = UIImage()
    var word = ""
}

