//
//  Profile.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/4/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class Profile {
    var image: String?
    var email: String?
    var phone: String?
    var category = [Categories]()
    init() {
        
    }
    
    init(json: JSON) {
        self.email = json["email"].stringValue
        self.phone = json["phone"].stringValue
        self.image = json["avatar"].stringValue
        self.category = json["categories"].arrayValue.compactMap( { Categories(json: $0) } )
    }
    
}
struct Categories {
    var id : Int
    var name : String
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["en_name"].stringValue
    }
}
