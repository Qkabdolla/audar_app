//
//  EntryModel.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import RealmSwift

class EntryModel: Object {
    
    @objc dynamic var inputText: String = ""
    @objc dynamic var outputText: String = ""
    
    convenience init(input: String, output: String) {
        self.init()
        self.inputText = input
        self.outputText = output
    }
}
