//
//  FontSize.swift
//  AudarApp
//
//  Created by Мадияр on 4/16/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class FontSize {
    class func superHuge() -> CGFloat { return Screen.width() * 41 / 375 }
    
    class func huge() -> CGFloat { return Screen.width() * 26 / 375 }
    
    class func medium() -> CGFloat { return Screen.width() * 18 / 375 }
    
    class func small() -> CGFloat { return Screen.width() * 16 / 375 }
    
    class func superSmall() -> CGFloat { return Screen.width() * 14 / 375 }
}

