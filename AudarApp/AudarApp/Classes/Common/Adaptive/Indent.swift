//
//  Indent.swift
//  AudarApp
//
//  Created by Мадияр on 4/16/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class Screen {
    class func width() -> CGFloat { return UIScreen.main.bounds.width }
    class func height() -> CGFloat { return UIScreen.main.bounds.height }
}

class Indent {
    class func offset() -> CGFloat { return Screen.width() * 0.042 }
    
    class func hugeOffset() -> CGFloat { return Screen.width() * 0.075 }
    
    class func superSmallOffset() -> CGFloat { return Screen.width() * 0.025 }
    
    class func superPuperSmallOffset() -> CGFloat { return Indent.superSmallOffset() / 2 }
}
