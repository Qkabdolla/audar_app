//
//  Animations.swift
//  AudarApp
//
//  Created by Мадияр on 3/27/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class Animations {
    static func requireUserAtencion(on onView: AnyObject) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: onView.center.x - 6, y: onView.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: onView.center.x + 6, y: onView.center.y))
        onView.layer.add(animation, forKey: "position")
    }
}
