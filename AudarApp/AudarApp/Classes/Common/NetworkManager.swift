//
//  NetworkManager.swift
//  AudarApp
//
//  Created by Мадияр on 4/23/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum ErrorType {
    case incorrectData
    case other
}

enum AlphabetType {
    case latin
    case cyrillic
}

class NetworkManager {
    
    func signin(username: String, password: String, success: @escaping () -> (), failure: @escaping (ErrorType) -> ()) {
        let parameters = [
            "username": "\(username)",
            "password": "\(password)"
        ]
        
        let requestString = Constants.Url.base + Constants.Methods.login
        
        AF.request(requestString, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                    if let token = json["accessToken"].string {
                        userDefaults.setUserToken(value: token)
                        userDefaults.setLoggedIn(value: true)
                        success()
                    } else {
                        failure(.incorrectData)
                    }
                    if let userName = json["username"].string, let email = json["email"].string {
                        userDefaults.setUserName(value: userName)
                        userDefaults.setUserEmail(value: email)
                    }
            case .failure(_):
                failure(.other)
            }
        }
    }
    
    func signup(username: String, email: String, password: String, success: @escaping () -> (), failure: @escaping (ErrorType) -> ()) {
        let parameters = [
            "username": "\(username)",
            "email": "\(email)",
            "password": "\(password)"
        ]
        
        let requestString = Constants.Url.base + Constants.Methods.reg
        
        AF.request(requestString, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json =  JSON(value)
                    if let _ = json["message"].string {
                        success()
                    } else {
                        failure(.incorrectData)
                    }
            case .failure(_):
                failure(.other)
            }
        }
    }
    
    func convertText(text: String, type: AlphabetType, success: @escaping (String) -> (), failure: @escaping (ErrorType) -> ()) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(userDefaults.getToken())"
        ]
        
        let parameters = [
            "text": "\(text)"
        ]
        
        let requestString: String!
        
        switch type {
        case .cyrillic: requestString = Constants.Url.base + Constants.Methods.convertCyr
        case .latin: requestString = Constants.Url.base + Constants.Methods.convertLat
        }
        
        AF.request(requestString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(headers)).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json =  JSON(value)
                    if let text = json["text"].string {
                        success(text)
                    } else {
                        failure(.other)
                    }
            case .failure(_):
                failure(.other)
            }
        }
    }
}
