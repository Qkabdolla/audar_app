//
//  RealmService.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//
import RealmSwift

class RealmService {
    
    private init() {}
    
    static let shared = RealmService()
    weak var delegate: UpdateTableViewDelegate?
    var realm = try! Realm()
    
    func create<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.add(object)
                delegate?.reloadTable()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
