//
//  NavProcess.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/6/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import Foundation
import UIKit

class Switcher {
    
    static func setConverter() {
        let rootVC = CustomTabBarController()
        rootVC.pushConverter()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = rootVC
        }
    }
    
    static func setConverter(text: String) {
        let rootVC = CustomTabBarController()
        rootVC.pushConverter(text: text)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = rootVC
        }
    }
    
    static func updateRootVC() {
        var rootVC : UIViewController?

        let viewController = SingInViewController()
        let tabar = CustomTabBarController()
    
        if(userDefaults.getLoggedIn()) {
            rootVC = tabar
        } else {
            rootVC = viewController
        }
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = rootVC
        }

    }
}
