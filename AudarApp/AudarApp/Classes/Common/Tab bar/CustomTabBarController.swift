//
//  CustomTabBar.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/3/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import Foundation
import UIKit


class CustomTabBarController:  UITabBarController, UITabBarControllerDelegate {

    var tabBarList: [UIViewController]?
    var inputText: String = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tabBar.barTintColor = UIColor.MyTheme.darkPurple
        tabBar.backgroundColor = UIColor.MyTheme.darkPurple
        tabBar.tintColor = UIColor.MyTheme.shinePurple
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .never
        
        self.delegate = self
        
        let orderViewController = UINavigationController(rootViewController: HomeViewController())
        orderViewController.tabBarItem.title = "Tools"
        orderViewController.tabBarItem.image = UIImage(named: "home")
        orderViewController.tabBarItem.selectedImage = UIImage(named: "selected-home")

        let responseViewController = UINavigationController(rootViewController: ConverterViewController())
        responseViewController.tabBarItem.title = "Convert"
        responseViewController.tabBarItem.image = UIImage(named: "convert")
        responseViewController.tabBarItem.selectedImage = UIImage(named: "selected-convert")
        
        let chatViewController = UINavigationController(rootViewController: CameraViewController())
        chatViewController.tabBarItem.title = "Camera"
        chatViewController.tabBarItem.image = UIImage(named: "camera")
        chatViewController.tabBarItem.selectedImage = UIImage(named: "selected-camera")
        
        let profileViewController = UINavigationController(rootViewController: ProfileViewController())
        profileViewController.tabBarItem.title = "Profile"
        profileViewController.tabBarItem.image = UIImage(named: "profile")
        profileViewController.tabBarItem.selectedImage = UIImage(named: "selected-profile")

        tabBarList = [orderViewController, responseViewController, chatViewController, profileViewController]
        viewControllers = tabBarList
        
        guard let items = tabBar.items else { return }
        
        for item in items {
            item.imageInsets = UIEdgeInsets(top: 2, left: 13, bottom: -2, right: 14)
        }
    }
    
    open func pushConverter() {
        selectedViewController = tabBarList![1]
    }
    open func pushConverter(text: String) {
        let navController = tabBarList![1] as! UINavigationController
        let vc = navController.topViewController as! ConverterViewController
        vc.rootView.inputText = text
        selectedViewController = navController
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

        let index = self.tabBar.items?.firstIndex(of: item)
        let subView = tabBar.subviews[index!+1].subviews.first as! UIImageView
        self.itemAnimation(imgView: subView)
        
    }

    func itemAnimation(imgView: UIImageView) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {

            imgView.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)

            UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                imgView.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) { (flag) in
            }
        }) { (flag) in

        }
    }
    
}
