//
//  CustomLaunchScreenSplashView.swift
//  AudarApp
//
//  Created by Мадияр on 4/6/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class LaunchScreenSplashView: UIView {
    
    //MARK: - Views
    private var iconImage: UIImage? {
        didSet{
            if let iconImage = self.iconImage{
                imageView?.image = iconImage
            }
        }
    }
    
    private var iconColor: UIColor = UIColor.white {
        didSet{
            imageView?.tintColor = iconColor
        }
    }
    
    private var iconInitialSize: CGSize = CGSize(width: 60, height: 60) {
        didSet{
             imageView?.frame = CGRect(x: 0, y: 0, width: iconInitialSize.width, height: iconInitialSize.height)
        }
    }

    private var imageView: UIImageView?

    private var duration: Double = 1.5
    
    private var delay: Double = 0.5
    
    //MARK: - init
    public init(iconImage: UIImage, iconInitialSize:CGSize, backgroundColor: UIColor) {
        self.imageView = UIImageView()
        self.iconImage = iconImage
        self.iconInitialSize = iconInitialSize
        super.init(frame: (UIScreen.main.bounds))
        
        imageView?.image = iconImage
        imageView?.tintColor = iconColor
        imageView?.frame = CGRect(x: 0, y: 0, width: iconInitialSize.width, height: iconInitialSize.height)
        imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        imageView?.center = self.center
        
        self.addSubview(imageView!)
        
        self.backgroundColor = backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public typealias SplashAnimatableCompletion = () -> Void
public typealias SplashAnimatableExecution = () -> Void

extension LaunchScreenSplashView {
    public func startAnimation(_ completion: SplashAnimatableCompletion? = nil) {
        playAnimation(completion)
    }
    
    private func playAnimation(_ completion: SplashAnimatableCompletion? = nil) {
        
        if let imageView = self.imageView {
            
            let shrinkDuration: TimeInterval = duration * 0.3
            
            UIView.animate(withDuration: shrinkDuration, delay: delay, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: UIView.AnimationOptions(), animations: {
               
                    let scaleTransform: CGAffineTransform = CGAffineTransform(scaleX: 0.75,y: 0.75)
                    imageView.transform = scaleTransform
                
                }, completion: { finished in
                    self.playZoomOutAnimation(completion)
            })
        }
    }
    
    private func playZoomOutAnimation(_ completion: SplashAnimatableCompletion? = nil) {
        if let imageView =  imageView {
            let growDuration: TimeInterval =  duration * 0.3
            
            UIView.animate(withDuration: growDuration, animations:{
                
                imageView.transform = self.getZoomOutTranform()
                self.alpha = 0
                
                }, completion: { finished in
                    self.removeFromSuperview()
                    completion?()
            })
        }
    }
    
    private func getZoomOutTranform() -> CGAffineTransform {
        let zoomOutTranform: CGAffineTransform = CGAffineTransform(scaleX: 20, y: 20)
        return zoomOutTranform
    }
}
