//
//  UserDefaults+Extension.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/5/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit

let userDefaults = UserDefaults.standard
extension UserDefaults {
    
    enum UserDefaultKeys: String {
        case userToken
        case isloggedIn
        case userName
        case userEmail
    }
    
    func setUserToken(value: String) {
        setValue(String(describing: value), forKey: UserDefaultKeys.userToken.rawValue)
    }
    
    func setUserName(value: String) {
        setValue(String(describing: value), forKey: UserDefaultKeys.userName.rawValue)
    }
    
    func setUserEmail(value: String) {
        setValue(String(describing: value), forKey: UserDefaultKeys.userEmail.rawValue)
    }
    
    func getToken() -> String {
        return string(forKey: UserDefaultKeys.userToken.rawValue) ?? ""
    }
    
    func getName() -> String {
        return string(forKey: UserDefaultKeys.userName.rawValue) ?? ""
    }
    
    func getEmail() -> String {
        return string(forKey: UserDefaultKeys.userEmail.rawValue) ?? ""
    }
    
    func setLoggedIn(value: Bool) {
        setValue(value, forKey: UserDefaultKeys.isloggedIn.rawValue)
    }
    
    func getLoggedIn() -> Bool {
        return bool(forKey: UserDefaultKeys.isloggedIn.rawValue)
    }
    
}
