//
//  UIColorExtension.swift
//  AudarApp
//
//  Created by Мадияр on 3/24/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

extension UIColor {
  struct MyTheme {
    static var darkPurple: UIColor  { return UIColor(red: 39.0/255, green: 30.0/255, blue: 76.0/255, alpha: 1.0) }
    static var lightPurple: UIColor { return UIColor(red: 50.0/255, green: 38.0/255, blue: 105.0/255, alpha: 1.0) }
    static var shinePurple: UIColor { return UIColor(red: 255/255, green: 16/255, blue: 239/255, alpha: 1.0) }
  }
}
