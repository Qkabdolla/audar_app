//
//  Const.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/5/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import Foundation

class Constants {
    struct Url {
        static let base = "http://195.2.67.2:8080/"
    }
    
    struct Methods {
        static let login = "api/auth/signin"
        static let reg = "api/auth/signup"
        static let convertCyr = "convert/cyr"
        static let convertLat = "convert/lat"
    }
}
