//
//  Alert.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

struct Alert {
    typealias ActionHandler = (_ action: UIAlertAction) -> Void
    
    private static func showBasicAlert(on vc: UIViewController,with title: String,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    private static func showBasicAlertWithHandler(on vc: UIViewController,with title: String,message: String, completioHandler: @escaping (_ action: UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: completioHandler))
        vc.present(alert, animated: true, completion: nil)
    }
    static func showFillMandatoryFields(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Заполните поле", message: "Пожалуйста заполните все нужные поля")
    }
    static func successSaveAlert(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Успешно сохранено", message: "Перейдите в избранные переводы,чтобы просмотреть ваши переводы")
    }
    static func unsuccesImageRecognition(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Упс!", message: "Приложение не смогло отсканировть текс, выберите другой снимок")
    }
    static func incorrectData(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Неверно", message: "Логин или пароль введены не правильно!")
    }
    static func otherError(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Ошибка", message: "Пожалуйста проверьте интернет подключение!")
    }
    static func passwordsIncorrect(vc: UIViewController){
        Alert.showBasicAlert(on: vc, with: "Ошибка", message: "Пароли не совпадают!")
    }
    static func succesSignUp(vc: UIViewController, completioHandler: @escaping (_ action: UIAlertAction) -> Void){
        Alert.showBasicAlertWithHandler(on: vc, with: "Супер!", message: "Вы успешно зарегистрировались", completioHandler: completioHandler)
    }
    
    static func showActionSheet(on vc: UIViewController, firstHandler: @escaping ActionHandler, secondHandler: @escaping ActionHandler, thirdHandler: @escaping ActionHandler) {
        let alert = UIAlertController(title: "Принимаем звонки с 10:00 до 19:00 будние дни", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Позвонить", style: .default, handler: firstHandler))
        alert.addAction(UIAlertAction(title: "Написать по Whatsup", style: .default, handler: secondHandler))
        alert.addAction(UIAlertAction(title: "Написать", style: .default, handler: thirdHandler))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showAlertWithOption(on vc: UIViewController, with title: String, message: String, noHandler: ((_ action: UIAlertAction) -> Void)?, completioHandler: @escaping (_ action: UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionDelete = UIAlertAction(title: "Дa", style: .default) { (action) in
            completioHandler(action)
        }
        let actionStop = UIAlertAction(title: "Нет", style: .cancel, handler: noHandler)
        alert.addAction(actionDelete)
        alert.addAction(actionStop)
        vc.present(alert, animated: true, completion: nil)
    }
}


