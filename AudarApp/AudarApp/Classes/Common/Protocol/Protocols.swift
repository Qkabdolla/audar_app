//
//  Protocols.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

protocol UpdateTableViewDelegate: class {
    func reloadTable()
}

protocol UpdateConverterPageDelegate: class {
    func setData()
    func showAlert()
}


