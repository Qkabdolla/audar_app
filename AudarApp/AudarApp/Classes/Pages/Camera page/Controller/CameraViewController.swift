//
//  CameraViewController.swift
//  AudarApp
//
//  Created by Мадияр on 3/24/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit
import MobileCoreServices
import TesseractOCR
import GPUImage

class CameraViewController: UIViewController, G8TesseractDelegate {
    
    var indicator = UIActivityIndicatorView()
    
    let rootView = BaseView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        navigationItem.title = "Scanning"
        updateConstraint()
        rootView.gallaryBtn.addTarget(self, action: #selector(openPhotoLibraries(_:)), for: .touchUpInside)
    }
    
    func updateConstraint() {
        view.addSubview(rootView)
        
        rootView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        
        view.addSubview(indicator)
        indicator.style = .large
        indicator.color = UIColor.MyTheme.shinePurple
        indicator.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.centerY.equalTo(view.snp.centerY)
            make.width.height.equalTo(50)
        }
    }
    
    @objc func openPhotoLibraries(_ sender: UIButton) {
        let imagePickerActionSheet = UIAlertController(title: "Snap/Upload Image", message: nil, preferredStyle: .actionSheet)

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(
              title: "Take Photo",
              style: .default) { (alert) -> Void in
                self.indicator.startAnimating()
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: {
                    self.indicator.stopAnimating()
                })
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(
            title: "Choose Existing",
            style: .default) { (alert) -> Void in
            self.indicator.startAnimating()
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker, animated: true, completion: {
                self.indicator.stopAnimating()
            })
        }
        imagePickerActionSheet.addAction(libraryButton)
           
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
           
        present(imagePickerActionSheet, animated: true)
    }
    
    func recognizeImageWithTesseract(_ image: UIImage) {
        let scaledImage = image.scaledImage(1000) ?? image
        let preprocessedImage = scaledImage.preprocessedImage() ?? scaledImage
            
        guard let tesseract = G8Tesseract(language: "kaz") else { return }
        tesseract.pageSegmentationMode = .auto
        tesseract.delegate = self
        tesseract.image = preprocessedImage
        tesseract.maximumRecognitionTime = 10.0
        tesseract.recognize()

        let text = tesseract.recognizedText ?? ""
        
        indicator.stopAnimating()
        
        switch checkResult(text: text) {
        case true:
            Switcher.setConverter(text: text)
        case false:
            Alert.unsuccesImageRecognition(vc: self)
        }
    }
    
    private func checkResult(text: String) -> Bool {
        let arr = ["ә", "і", "ң", "ғ", "ү", "ұ", "қ", "ө", "h"]
        
        for i in text {
            if arr.contains(String(i)) {
                return true
            }
        }
        return false
    }
}

extension CameraViewController: UINavigationControllerDelegate { }

// MARK: - UIImagePickerControllerDelegate
extension CameraViewController: UIImagePickerControllerDelegate {
    
  func imagePickerController(_ picker: UIImagePickerController,
       didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let selectedPhoto = info[.originalImage] as? UIImage else {
        
        dismiss(animated: true)
        return
    }
    indicator.startAnimating()
    dismiss(animated: true) {
        self.recognizeImageWithTesseract(selectedPhoto)
    }
  }
}

extension UIImage {
    
  func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
    
    var scaledSize = CGSize(width: maxDimension, height: maxDimension)

    if size.width > size.height {
      scaledSize.height = size.height / size.width * scaledSize.width
        
    } else {
        
      scaledSize.width = size.width / size.height * scaledSize.height
        
    }

    UIGraphicsBeginImageContext(scaledSize)
    draw(in: CGRect(origin: .zero, size: scaledSize))
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return scaledImage
  }
  
  func preprocessedImage() -> UIImage? {
    let stillImageFilter = GPUImageAdaptiveThresholdFilter()
    stillImageFilter.blurRadiusInPixels = 15.0
    let filteredImage = stillImageFilter.image(byFilteringImage: self)
    return filteredImage
  }
}
