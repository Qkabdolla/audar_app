//
//  BaseView.swift
//  AudarApp
//
//  Created by Koltubayev on 4/13/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = UIColor.MyTheme.lightPurple
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        addSubview(subtitleLabel)
        subtitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Indent.offset())
            make.centerX.equalToSuperview()
        }
        
        let size = Screen.height() * 0.3
        
        addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            if #available(iOS 11, *) {
                make.centerX.equalToSuperview()
                make.width.height.equalTo(size)
                make.bottom.equalTo(titleLabel.snp.top).offset(-Indent.offset())
            } else {
                make.width.height.equalTo(120)
                make.centerX.equalToSuperview()
                make.bottom.equalTo(titleLabel.snp.top).offset(-Indent.offset())
            }
        }
        
        addSubview(gallaryBtn)
        gallaryBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(snp.centerX).offset(150)
            make.width.height.equalTo(60)
            make.bottom.equalTo(-20)
        }
        
    }
    
    var imageView : UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = UIImage(named: "IconCameraPage")
        img.tintColor = .white
        return img
    }()
    
    var titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir-Next", size: 24)
        lbl.font = .boldSystemFont(ofSize: 24)
        lbl.text = "There is no scanned documents"
        lbl.numberOfLines = 0
        return lbl
    }()
    
    var subtitleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir-Next", size: 18)
        lbl.text = "Scan the new document -\n concern the shooting button below"
        lbl.numberOfLines = 0
        return lbl
    }()
    
    var gallaryBtn : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.MyTheme.darkPurple
        button.clipsToBounds = true
        button.layer.cornerRadius = 30
        button.tintColor = .white
        button.setImage(UIImage(named: "cameraPage"), for: .normal)
        return button
    }()
}
