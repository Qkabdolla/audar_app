//
//  FirstViewController.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 1/9/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit
import SnapKit

class FirstViewController: UIViewController {
    
    let networkManager = NetworkManager()
    
//    var rootView: SignUpView { return self.view as! SignUpView}
    let rootView = SignUpView()
    
//    override func loadView() {
//        self.view = SignUpView()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurView()
        
        view.addSubview(rootView)
        rootView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func configurView() {
        rootView.registrationButton.addTarget(self, action: #selector(dataTranfer(_:)), for: .touchUpInside)
        rootView.showButton.addTarget(self, action: #selector(pushController), for: .touchUpInside)
    }
    
    @objc func pushController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func presentController() {
        let vc = CustomTabBarController()
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func dataTranfer(_ sender: UIButton) {
        rootView.activityView.startAnimating()
        guard let username = rootView.emailtextFields.text, let email = rootView.phonetextFields.text, let password1 = rootView.passwordtextFields1.text, let password2 = rootView.passwordtextFields2.text else { return }
        
        if username == "" || password1 == "" || password2 == "" || email == "" {
            rootView.activityView.stopAnimating()
            Alert.showFillMandatoryFields(vc: self)
            return
        }
        
        if password1 != password2 {
            rootView.activityView.stopAnimating()
            Alert.passwordsIncorrect(vc: self)
            return
        }
        
        networkManager.signup(username: username, email: email, password: password1, success: {
            self.rootView.activityView.stopAnimating()
            Alert.succesSignUp(vc: self) { [weak self] (_) in
                self?.pushController()
            }
            
        }) { (error) in
            self.rootView.activityView.stopAnimating()
            switch error {
            case .incorrectData: Alert.incorrectData(vc: self)
            case .other: Alert.otherError(vc: self)
            }
        }

    }
}

