//
//  RegistrationView.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 1/30/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit
import SnapKit

class SignUpView: UIView{
    
    init() {
        super.init(frame: .zero)
        updateConstraint()
        
        emailtextFields.delegate = self
        phonetextFields.delegate = self
        passwordtextFields1.delegate = self
        passwordtextFields2.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing(true)
    }
    
    func updateConstraint() {
        
        addSubview(backGroundViews)
        backGroundViews.snp.makeConstraints { make in
            make.edges.equalToSuperview()
       }
        
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
          if #available(iOS 11, *) {
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).offset(25)
            make.centerX.equalToSuperview()
            make.size.equalTo(150)
          } else {
            make.top.equalTo(25)
            make.centerX.equalToSuperview()
            make.size.equalTo(150)
          }
        }
        
        addSubview(emailtextFields)
        emailtextFields.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(30)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(phonetextFields)
        phonetextFields.snp.makeConstraints { (make) in
            make.top.equalTo(emailtextFields.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(passwordtextFields1)
        passwordtextFields1.snp.makeConstraints { (make) in
            make.top.equalTo(phonetextFields.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(passwordtextFields2)
        passwordtextFields2.snp.makeConstraints { (make) in
            make.top.equalTo(passwordtextFields1.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(registrationButton)
        registrationButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordtextFields2.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(orLabel)
        orLabel.snp.makeConstraints { (make) in
            make.top.equalTo(registrationButton.snp.bottom).offset(16)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(showButton)
        showButton.snp.makeConstraints { (make) in
            make.top.equalTo(orLabel.snp.bottom).offset(16)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(activityView)
        activityView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(110)
            make.centerX.equalTo(imageView.snp.centerX)
        }
    }
    
    var backGroundViews : UIView = {
        let  views = UIView()
        views.backgroundColor = UIColor.MyTheme.darkPurple
        return views
    }()
    
    var imageView : UIImageView = {
        let icons = UIImageView()
        icons.backgroundColor = .gray
        icons.image = UIImage(named: "appstore")
        return icons
    }()
    
    var emailtextFields : UITextField = {
        let tf = UITextField()
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor.MyTheme.darkPurple
        tf.textColor = .white
        tf.placeholder = "username"
        tf.placeholderColor(.white)
        tf.autocapitalizationType = .none
        tf.autocorrectionType = .no
        tf.returnKeyType = .done
        return tf
    }()
    
    var phonetextFields : UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.MyTheme.darkPurple
        tf.textColor = .white
        tf.borderStyle = .roundedRect
        tf.placeholder = "email"
        tf.placeholderColor(.white)
        tf.returnKeyType = .done
        return tf
    }()
    var passwordtextFields1 : UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.MyTheme.darkPurple
        tf.textColor = .white
        tf.borderStyle = .roundedRect
        tf.placeholder = "Password"
        tf.placeholderColor(.white)
        tf.autocapitalizationType = .none
        tf.autocorrectionType = .no
        tf.isSecureTextEntry = true
        tf.returnKeyType = .done
        return tf
    }()
    
    var passwordtextFields2 : UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.MyTheme.darkPurple
        tf.textColor = .white
        tf.borderStyle = .roundedRect
        tf.placeholder = "Confirm password"
        tf.placeholderColor(.white)
        tf.autocapitalizationType = .none
        tf.autocorrectionType = .no
        tf.isSecureTextEntry = true
        tf.returnKeyType = .done
        return tf
    }()
    
    var registrationButton : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 233.0/255, green: 63.0/255, blue: 137.0/255, alpha: 1.0)
        button.layer.cornerRadius  = 5
        button.tintColor = .white
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        button.setTitle("SING UP", for: UIControl.State.normal)
        return button
    }()
    
    var orLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir Next", size: 17)
        lbl.text = "or"
        return lbl
    }()
    
    var showButton : UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .lightGray
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 16)
        button.setTitle("I have already account", for: UIControl.State.normal)
        return button
    }()
    
    var activityView : UIActivityIndicatorView = {
        let av = UIActivityIndicatorView()
        av.style = .large
        av.color = .magenta
        return av
    }()
    
}

extension SignUpView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
}

extension UITextField {
    
    func placeholderColor(_ color: UIColor) {
        var placeholderText = ""
        if self.placeholder != nil {
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}
