//
//  EmptyView.swift
//  AudarApp
//
//  Created by Мадияр on 4/20/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    lazy private var logoView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "star_big")?.withRenderingMode(.alwaysTemplate)
        view.tintColor = .white
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Next", size: 24)
        label.font = .boldSystemFont(ofSize: 24)
        label.textAlignment = .center
        label.textColor = .white
        label.text = "You do not have data yet"
        return label
    }()

    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        addSubview(logoView)
        addSubview(titleLabel)

        titleLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        logoView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(titleLabel.snp.top).offset(-Indent.hugeOffset())
            make.size.equalTo(100)
        }
    }
}
