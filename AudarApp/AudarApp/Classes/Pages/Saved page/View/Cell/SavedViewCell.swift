//
//  SavedViewCell.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class SavedViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    private let cardView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MyTheme.darkPurple
        view.layer.cornerRadius = 8
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.medium())
        label.numberOfLines = 3
        label.textColor = .white
        return label
    }()
   
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        label.numberOfLines = 3
        label.textColor = .lightGray
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
    }
    
    // MARK: - Helper function
    
    private func layoutUI() {
        addSubview(cardView)
        cardView.addSubview(titleLabel)
        cardView.addSubview(descriptionLabel)
        
        cardView.anchor(top: topAnchor,
                        left: leftAnchor,
                        bottom: bottomAnchor,
                        right: rightAnchor,
                        paddingTop: Indent.superPuperSmallOffset(),
                        paddingLeft: Indent.superPuperSmallOffset(),
                        paddingBottom: 0,
                        paddingRight: Indent.superPuperSmallOffset())
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Indent.superSmallOffset())
            make.leading.trailing.equalToSuperview().inset(Indent.superSmallOffset())
        }
        
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Indent.superSmallOffset())
            make.leading.trailing.equalToSuperview().inset(Indent.superSmallOffset())
            make.bottom.equalToSuperview().offset(-Indent.superSmallOffset())
        }
    }
    
    func configureCell(_ entry: EntryModel) {
        titleLabel.text = entry.inputText
        descriptionLabel.text = entry.outputText
    }
    
    // MARK: - Хуйня
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
