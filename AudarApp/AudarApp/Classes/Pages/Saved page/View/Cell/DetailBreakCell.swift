//
//  DetailBreakeCell.swift
//  AudarApp
//
//  Created by Мадияр on 5/7/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class DetailBreakCell: UICollectionViewCell {
    // MARK: - Properties
    
    let breakView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MyTheme.shinePurple
        return view
    }()
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.MyTheme.darkPurple
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    
    private func configureUI() {
        addSubview(breakView)
        breakView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Indent.superSmallOffset())
            make.height.equalTo(0.8)
        }
    }
}
