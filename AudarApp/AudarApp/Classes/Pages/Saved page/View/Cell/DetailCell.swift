//
//  DetailCell.swift
//  AudarApp
//
//  Created by Мадияр on 5/7/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class DetailCell: UICollectionViewCell {
    // MARK: - Properties
    
    var inputText = "" {
        didSet {
            inputTextView.text = inputText
        }
    }
    
    let inputTextView: UITextView = {
        let text = UITextView()
        text.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        let inset = Indent.superSmallOffset()
        text.contentInset = UIEdgeInsets(top: inset - 10,
                                         left: inset,
                                         bottom: inset,
                                         right: inset)
        text.textColor = .white
        text.layer.cornerRadius = 8
        text.isEditable = false
        text.isScrollEnabled = false
        text.backgroundColor = UIColor.MyTheme.darkPurple
        return text
    }()
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.MyTheme.darkPurple
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    
    private func configureUI() {
        addSubview(inputTextView)
        inputTextView.snp.makeConstraints { (make) in
            make.top.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
