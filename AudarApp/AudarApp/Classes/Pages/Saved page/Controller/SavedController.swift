//
//  SavedController.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit
import RealmSwift

private let cellId = "cellId"

class SavedController: UIViewController {
    
    // MARK: - Properties
    
    var tableView = UITableView()
    let detailController = DetailViewController(collectionViewLayout: UICollectionViewFlowLayout())
    var entriesList: Results<EntryModel>!
    let tableEmptyView = EmptyView()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        getEntries()
        
        configureUI()
        configureNavigation()
        configureTableView()
        layoutEmptyView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Helper function
    
    func getEntries() {
        entriesList = RealmService.shared.realm.objects(EntryModel.self)
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        tableView.backgroundColor = UIColor.MyTheme.lightPurple
        tableView.backgroundView = nil
        tableView.register(SavedViewCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    private func configureUI() {
        view.backgroundColor = UIColor.MyTheme.lightPurple
        RealmService.shared.delegate = self
        detailController.delegate = self
    }
    
    private func configureNavigation() {
        navigationItem.title = "Saved"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func layoutEmptyView() {
        view.addSubview(tableEmptyView)
        tableEmptyView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        checkIsEmpty()
    }
    
    private func checkIsEmpty() {
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableEmptyView.isHidden = false
        } else {
            tableEmptyView.isHidden = true
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension SavedController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SavedViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        if let entry = entriesList?[indexPath.row] {
            cell.configureCell(entry)
        }
        return cell
    }
    
}

extension SavedController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(detailController, animated: true)
        if let entry = entriesList?[indexPath.row] {
            detailController.entry = entry
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        guard let entry = entriesList?[indexPath.row] else {
            return nil
        }
        
        let deleteAction = UIContextualAction(style: .destructive, title: "") { ( _,_, complete) in
            
            Alert.showAlertWithOption(on: self, with: "Вы уверены?", message: "вы точно хотите удалить?", noHandler: { (_) in
                complete(true)
            }) { [weak self] (_)  in 
                RealmService.shared.delete(entry)
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
                self?.reloadTable()
                complete(true)
            }
        }
        
        let view = UIImageView()
        view.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        view.image = UIImage(named: "bin")?.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.MyTheme.shinePurple
        view.backgroundColor = .clear
        
        deleteAction.backgroundColor = UIColor.MyTheme.lightPurple
        deleteAction.image = UIImage(view: view)
        
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        config.performsFirstActionWithFullSwipe = false
        
        return config
    }
    
}

// MARK: - updateTableViewDelegate

extension SavedController: UpdateTableViewDelegate {
    func reloadTable() {
        tableView.reloadData()
        checkIsEmpty()
    }
}
