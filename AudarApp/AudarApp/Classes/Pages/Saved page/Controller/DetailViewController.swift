//
//  DetailViewController.swift
//  AudarApp
//
//  Created by rau4o on 4/19/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class DetailViewController: UICollectionViewController {
    
    // MARK: - Properties
    
    var entry = EntryModel()
    weak var delegate: UpdateTableViewDelegate?
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        configureCollectionView()
        
    }
    
    // MARK: - Helper function
    
    private func configureCollectionView() {
        collectionView.register(DetailCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(DetailBreakCell.self, forCellWithReuseIdentifier: "breakCell")
        collectionView.backgroundColor = UIColor.MyTheme.lightPurple
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        let inset = Indent.superPuperSmallOffset()
        collectionView.contentInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
    }
    
    private func setupNavBar() {
        let delete = UIBarButtonItem(image: UIImage(named: "bin")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(handleDeleteButton(_:)))
        delete.tintColor = UIColor.MyTheme.shinePurple
        navigationItem.rightBarButtonItem = delete
    }
    
    private func size(forWidth width: CGFloat, row: Int) -> CGSize {
        let label = UILabel()
        if row == 0 {
            label.text = entry.inputText
        } else {
            label.text = entry.outputText
        }
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.anchor(width: width)
        return label.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    // MARK: - Selectors
    
    @objc private func handleDeleteButton(_ sender: UIBarButtonItem) {
        Alert.showAlertWithOption(on: self, with: "Вы уверены?", message: "вы точно хотите удалить?", noHandler: nil) { [weak self] (action) in
            RealmService.shared.delete((self?.entry)!)
            self?.delegate?.reloadTable()
            self?.navigationController?.popViewController(animated: true)
        }
    }
}

extension DetailViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DetailCell
            cell.inputText = entry.inputText
            cell.roundCorners(corners: [.topRight, .topLeft], radius: 8)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "breakCell", for: indexPath) as! DetailBreakCell
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DetailCell
            cell.inputText = entry.outputText
            cell.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            return cell
        default:
            break
        }
        
        return UICollectionViewCell()
    }
    
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 1 {
            return CGSize(width: view.frame.width - 10, height: 20)
        }
        
        let height = size(forWidth: view.frame.width - 10, row: indexPath.row).height
        return CGSize(width: view.frame.width - 10, height: height + 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
