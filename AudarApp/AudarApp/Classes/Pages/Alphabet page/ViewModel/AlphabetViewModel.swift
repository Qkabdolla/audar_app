//
//  AlphabetViewModel.swift
//  AudarApp
//
//  Created by rau4o on 4/6/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class AlphabetViewModel {
    
    var alphabetData: [AlphabetCard] = {
        var arr = [AlphabetCard]()
        
        var arr1 = AlphabetCard()
        arr1.letter = "Aa"
        arr1.imageName = UIImage(named: "Alphabet Images/alma")!
        arr1.word = "Alma"
        
        var arr2 = AlphabetCard()
        arr2.letter = "A'a"
        arr2.imageName = UIImage(named: "Alphabet Images/a'n")!
        arr2.word = "A'n"
        
        var arr3 = AlphabetCard()
        arr3.letter = "Bb"
        arr3.imageName = UIImage(named: "Alphabet Images/ba'sheshek")!
        arr3.word = "Baishehek"
        
        var arr4 = AlphabetCard()
        arr4.letter = "Dd"
        arr4.imageName = UIImage(named: "Alphabet Images/dorba")!
        arr4.word = "Dorba"
        
        var arr5 = AlphabetCard()
        arr5.letter = "Ee"
        arr5.imageName = UIImage(named: "Alphabet Images/eli'k")!
        arr5.word = "Eli'k"
        
        var arr6 = AlphabetCard()
        arr6.letter = "Ff"
        arr6.imageName = UIImage(named: "Alphabet Images/fonar'")!
        arr6.word = "Fonar'"
        
        var arr7 = AlphabetCard()
        arr7.letter = "Gg"
        arr7.imageName = UIImage(named: "Alphabet Images/gul")!
        arr7.word = "Gul"
        
        var arr8 = AlphabetCard()
        arr8.letter = "G'g'"
        arr8.imageName = UIImage(named: "Alphabet Images/g'arysh")!
        arr8.word = "G'arysh"
        
        var arr9 = AlphabetCard()
        arr9.letter = "Hh"
        arr9.imageName = UIImage(named: "Alphabet Images/habar")!
        arr9.word = "Habar"
        
        var arr11 = AlphabetCard()
        arr11.letter = "Ii"
        arr11.imageName = UIImage(named: "Alphabet Images/ine")!
        arr11.word = "Ine"
        
        var arr12 = AlphabetCard()
        arr12.letter = "II"
        arr12.imageName = UIImage(named: "Alphabet Images/ilmek")!
        arr12.word = "Ilmek"
        
        var arr13 = AlphabetCard()
        arr13.letter = "Jj"
        arr13.imageName = UIImage(named: "Alphabet Images/zhalau")!
        arr13.word = "Jalau"
        
        var arr14 = AlphabetCard()
        arr14.letter = "Kk"
        arr14.imageName = UIImage(named: "Alphabet Images/ki'im")!
        arr14.word = "Ki'im"
        
        var arr15 = AlphabetCard()
        arr15.letter = "Ll"
        arr15.imageName = UIImage(named: "Alphabet Images/lalagul")!
        arr15.word = "Lalagul"
        
        var arr16 = AlphabetCard()
        arr16.letter = "Mm"
        arr16.imageName = UIImage(named: "Alphabet Images/makta")!
        arr16.word = "Makta"
        
        var arr17 = AlphabetCard()
        arr17.letter = "Nn"
        arr17.imageName = UIImage(named: "Alphabet Images/nan")!
        arr17.word = "Nan"
        
        var arr18 = AlphabetCard()
        arr18.letter = "N'n'"
        arr18.imageName = UIImage(named: "Alphabet Images/tanym")!
        arr18.word = "Tan'ym"
        
        var arr19 = AlphabetCard()
        arr19.letter = "Oo"
        arr19.imageName = UIImage(named: "Alphabet Images/otan")!
        arr19.word = "Otan"
        
        var arr21 = AlphabetCard()
        arr21.letter = "Pp"
        arr21.imageName = UIImage(named: "Alphabet Images/parta")!
        arr21.word = "Parta"
        
        var arr22 = AlphabetCard()
        arr22.letter = "Qq"
        arr22.imageName = UIImage(named: "Alphabet Images/konyrau")!
        arr22.word = "Qonyrau"
        
        var arr23 = AlphabetCard()
        arr23.letter = "Rr"
        arr23.imageName = UIImage(named: "Alphabet Images/r")!
        arr23.word = "R"
        
        var arr24 = AlphabetCard()
        arr24.letter = "Ss"
        arr24.imageName = UIImage(named: "Alphabet Images/sulgi")!
        arr24.word = "Sulgi'"
        
        var arr25 = AlphabetCard()
        arr25.letter = "Tt"
        arr25.imageName = UIImage(named: "Alphabet Images/tulki")!
        arr25.word = "Tulki"
        
        var arr26 = AlphabetCard()
        arr26.letter = "Uu"
        arr26.imageName = UIImage(named: "Alphabet Images/uys")!
        arr26.word = "Uys"
        
        var arr27 = AlphabetCard()
        arr27.letter = "U'u'"
        arr27.imageName = UIImage(named: "Alphabet Images/uly")!
        arr27.word = "u'ly"
        
        var arr28 = AlphabetCard()
        arr28.letter = "Vv"
        arr28.imageName = UIImage(named: "Alphabet Images/vagon")!
        arr28.word = "Vagon"
        
        var arr29 = AlphabetCard()
        arr29.letter = "Yy"
        arr29.imageName = UIImage(named: "Alphabet Images/uirek")!
        arr29.word = "Uirek"
        
        var arr31 = AlphabetCard()
        arr31.letter = "Y'y'"
        arr31.imageName = UIImage(named: "Alphabet Images/ydys")!
        arr31.word = "Ydys"
        
        var arr32 = AlphabetCard()
        arr32.letter = "Zz"
        arr32.imageName = UIImage(named: "Alphabet Images/zhalau")!
        arr32.word = "Zhalau"
        
        arr.append(arr1)
        arr.append(arr2)
        arr.append(arr3)
        arr.append(arr4)
        arr.append(arr5)
        arr.append(arr6)
        arr.append(arr7)
        arr.append(arr8)
        arr.append(arr9)
        arr.append(arr11)
        arr.append(arr12)
        arr.append(arr13)
        arr.append(arr14)
        arr.append(arr15)
        arr.append(arr16)
        arr.append(arr17)
        arr.append(arr18)
        arr.append(arr19)
        arr.append(arr21)
        arr.append(arr22)
        arr.append(arr23)
        arr.append(arr24)
        arr.append(arr25)
        arr.append(arr26)
        arr.append(arr27)
        arr.append(arr28)
        arr.append(arr29)
        arr.append(arr31)
        arr.append(arr32)
        return arr
    }()
    
    func numberOfElements() -> Int {
        return alphabetData.count
    }
    
    func getElements(at row: Int) -> AlphabetCard{
        return alphabetData[row]
    }
}
