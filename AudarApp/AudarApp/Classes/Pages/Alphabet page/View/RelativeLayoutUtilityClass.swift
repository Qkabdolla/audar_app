//
//  RelativeLayoutUtilityClass.swift
//  AudarApp
//
//  Created by rau4o on 4/6/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class RelativeLayoutUtilityClass {
    var heightFrame: CGFloat?
    var widthFrame: CGFloat?

    init(referenceFrameSize: CGSize){
        heightFrame = referenceFrameSize.height
        widthFrame = referenceFrameSize.width
    }
    
    func relativeHeight(multiplier: CGFloat) -> CGFloat{

        return multiplier * self.heightFrame!
    }
    
    func relativeWidth(multiplier: CGFloat) -> CGFloat{
        return multiplier * self.widthFrame!
    }
}
