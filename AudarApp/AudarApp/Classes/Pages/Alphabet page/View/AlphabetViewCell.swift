//
//  AlphabetViewCell.swift
//  AudarApp
//
//  Created by rau4o on 4/6/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class AlphabetViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    var cardView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MyTheme.darkPurple
        view.layer.cornerRadius = 15
        return view
    }()
    
    var imageView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .red
        image.isHidden = true
        return image
    }()
    
    var wordLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 35)
        label.isHidden = true
        label.textAlignment = .center
        label.minimumScaleFactor = 0.8
        label.numberOfLines = 2
        label.sizeToFit()
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var letterLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 50)
        label.textAlignment = .center
        return label
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    // MARK: - Helper function
    
    private func layoutUI() {
        addSubview(cardView)
        cardView.addSubview(letterLabel)
        cardView.addSubview(imageView)
        cardView.addSubview(wordLabel)
        
        cardView.centerY(inView: self)
        cardView.centerX(inView: self)
        cardView.setDimension(height: 350, width: 350)
        
        letterLabel.centerY(inView: cardView)
        letterLabel.centerX(inView: cardView)
        letterLabel.setDimension(height: 200, width: 200)
        
        imageView.centerY(inView: cardView)
        imageView.centerX(inView: cardView)
        imageView.setDimension(height: 200, width: 200)
        
        wordLabel.anchor(top: imageView.bottomAnchor, left: imageView.leftAnchor, bottom: nil, right: imageView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0,height: 50)
    }
    
    func configureCell(alphabetCard: AlphabetCard) {
        imageView.image = alphabetCard.imageName
        letterLabel.text = alphabetCard.letter
        wordLabel.text = alphabetCard.word
    }
    
    func checkFlip(bool: Bool) {
        imageView.isHidden = !bool
        wordLabel.isHidden = !bool
        letterLabel.isHidden = bool
    }
    
    // MARK: - Flip cardView
    
    func flip() {
        imageView.isHidden = false
        wordLabel.isHidden = false
        letterLabel.isHidden = true
        UIView.transition(with: cardView, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: nil)
    }
    
    func backFlip() {
        imageView.isHidden = true
        wordLabel.isHidden = true
        letterLabel.isHidden = false
        UIView.transition(with: cardView, duration: 0.3, options: .transitionFlipFromRight, animations: nil, completion: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
