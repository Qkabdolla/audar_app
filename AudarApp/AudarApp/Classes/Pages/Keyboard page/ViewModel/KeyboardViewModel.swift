//
//  KeyboardViewModel.swift
//  AudarApp
//
//  Created by Мадияр on 4/4/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

class KeyboardViewModel {
    
    private let data: [KeyboardSettingModel] = [
        KeyboardSettingModel(number: 1, imageName: "settings-app", title: "Go to Settings", subTitle: ""),
        KeyboardSettingModel(number: 2, imageName: "", title: "Audar App", subTitle: ""),
        KeyboardSettingModel(number: 3, imageName: "keyboard-app", title: "Keyboard", subTitle: ""),
        KeyboardSettingModel(number: 4, imageName: "", title: "Enable Kaz Keyboard", subTitle: ""),
        KeyboardSettingModel(number: 5, imageName: "", title: "Allow full access", subTitle: "Optional"),
        KeyboardSettingModel(number: 6, imageName: "", title: "Come back here to use our app", subTitle: ""),
    ]
    
    func numberOfElements() -> Int {
        return data.count
    }
    
    func getElement(at row: Int) -> KeyboardSettingModel {
        return data[row]
    }
}

struct KeyboardSettingModel {
    let number: Int
    let imageName: String
    let title: String
    let subTitle: String
}
