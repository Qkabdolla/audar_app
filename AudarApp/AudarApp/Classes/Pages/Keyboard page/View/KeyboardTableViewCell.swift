//
//  KeyboardTableViewCell.swift
//  AudarApp
//
//  Created by Мадияр on 4/4/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class KeyboardTableViewCell: UITableViewCell {
    
    //MARK: - Views
    
    var titleText = "" {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    var subText = "" {
        didSet {
            subTitleLabel.text = subText
            if subText == "" {
                labelStackView.removeArrangedSubview(subTitleLabel)
            }
        }
    }
    
    var imageNameText = "" {
        didSet {
            if imageNameText == "" {
                stackView.removeArrangedSubview(mainImageView)
            } else {
                mainImageView.image = UIImage(named: imageNameText)
            }
        }
    }
    
    var numberText = 1 {
        didSet {
            numberLabel.text = "\(numberText)"
        }
    }
    
    private let numberLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: FontSize.medium())
        return label
    }()
    
    lazy private var numberView: UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(white: 1, alpha: 0)
            view.layer.borderWidth = 2
            view.layer.borderColor = UIColor.MyTheme.shinePurple.cgColor
            view.layer.cornerRadius =  Screen.height() * 0.04 / 2
            return view
    }()

    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        return label
    }()
    
    lazy private var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont(name: "Roboto-Regular", size: 12)
        return label
    }()
    
    lazy private var labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subTitleLabel)
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.spacing = 3
        return stackView
    }()
    
    lazy private var mainImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [numberView, mainImageView, labelStackView])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.spacing = 20
        stackView.autoresizesSubviews = true
        return stackView
    }()
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.MyTheme.lightPurple
        isUserInteractionEnabled = false
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func layoutUI() {
        
        addSubviews([stackView])
        
        stackView.anchor(top: self.topAnchor,
                         left: self.leftAnchor,
                         bottom: self.bottomAnchor,
                         right: self.rightAnchor,
                         paddingTop: Screen.height() * 0.025,
                         paddingLeft: Indent.offset(),
                         paddingBottom: Screen.height() * 0.025,
                         paddingRight: Indent.offset())
        
        mainImageView.setDimension(height: Screen.height() * 0.04, width: Screen.height() * 0.04)
        
        numberView.setDimension(height: Screen.height() * 0.04, width: Screen.height() * 0.04)
        
        numberView.addSubview(numberLabel)
        
        numberLabel.centerX(inView: numberView)
        numberLabel.centerY(inView: numberView)
    }
    
    private func addSubviews(_ view: [UIView]) {
        for i in view {
            addSubview(i)
        }
    }
}
