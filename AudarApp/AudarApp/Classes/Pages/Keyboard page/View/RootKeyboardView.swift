//
//  RootKeyboardView.swift
//  AudarApp
//
//  Created by Мадияр on 4/1/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class RootKeyboardView: UIView {
    
    // MARK: - Views
    
    lazy private var mainLabel: UILabel = {
        let label = UILabel()
        label.text = "Welcome!"
        label.textColor = .white
        label.font = UIFont(name: "Montserrat-Bold", size: FontSize.huge())
        return label
    }()
    
    lazy private var subLabel: UILabel = {
        let label = UILabel()
        label.text = "To enable Kazakh keyboard do the following"
        label.textColor = .lightGray
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.superSmall())
        return label
    }()
    
    lazy private var topStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [mainLabel, subLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        return stackView
    }()
    
    // MARK: - Init
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor.MyTheme.lightPurple
        
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - private functions
    
    private func layoutUI() {
        addSubview(topStackView)
        
        topStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
