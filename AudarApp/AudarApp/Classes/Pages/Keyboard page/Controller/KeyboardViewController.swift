//
//  KeyboardViewController.swift
//  AudarApp
//
//  Created by Мадияр on 4/1/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class KeyboardViewController: UIViewController {
    
    // MARK: - Views
    let rootView = RootKeyboardView()
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.isUserInteractionEnabled = false
        tableView.backgroundColor = UIColor.MyTheme.lightPurple
        
        tableView.register(KeyboardTableViewCell.self, forCellReuseIdentifier: "Cell")
        return tableView
    }()
    
    lazy private var settingsButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setTitle("Go to settings!", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        button.addTarget(self, action: #selector(didSettingsTapped), for: .touchUpInside)
        return button
    }()
    
    let viewModel = KeyboardViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        
        layoutUI()
    }
    
    // MARK: - Layout UI & Configure
    private func layoutUI() {
        view.addSubview(rootView)
        view.addSubview(tableView)
        view.addSubview(settingsButton)
        
        rootView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Indent.hugeOffset())
            make.leading.trailing.equalToSuperview()
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(rootView.snp.bottom).offset(Screen.height() * 0.15)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-Screen.height() * 0.1)
        }
        
        settingsButton.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(Indent.superSmallOffset())
            make.centerX.equalToSuperview()
        }
    }
    
    @objc private func didSettingsTapped() {
        guard let settingsURL = URL.init(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
    }
}

extension KeyboardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfElements()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! KeyboardTableViewCell
        let item = viewModel.getElement(at: indexPath.row)
        
        cell.titleText = item.title
        cell.subText = item.subTitle
        cell.imageNameText = item.imageName
        cell.numberText = item.number
        
        return cell
    }
}
