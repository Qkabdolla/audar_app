//
//  rootConverterView.swift
//  AudarApp
//
//  Created by Мадияр on 3/26/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class RootConverterView: UIView {
    
    let maxCharacter: Int = 200
    
    //MARK: - Views
    
    var inputText = "" {
        didSet {
            inputTextView.text = inputText
        }
    }
    
    var outputText = "" {
        didSet {
            outputTextView.text = outputText
        }
    }
    
    lazy private var inputTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: FontSize.small())
        textView.textColor = .white
        textView.backgroundColor = UIColor.MyTheme.darkPurple
        textView.addInputAccessoryView(style: .done, target: self, selector:  #selector(didDoneTap))
        textView.autocorrectionType = .no
        return textView
    }()
    
    lazy private var counterLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .lightGray
        label.text = "0/200"
        return label
    }()
    
    lazy private var breakLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MyTheme.shinePurple
        return view
    }()
    
    lazy private var outputTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: FontSize.small())
        textView.textColor = .white
        textView.backgroundColor = UIColor.MyTheme.darkPurple
        textView.isEditable = false
        return textView
    }()
    
    var didDoneTapped: (() -> Void)?
    
    // MARK: - Init
    init() {
        super.init(frame: .zero)
        inputTextView.delegate = self
        
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing(true)
    }
    
    @objc private func didDoneTap() {
        self.endEditing(true )
        inputText = inputTextView.text
        didDoneTapped?()
    }
    
    private func layoutUI() {
        backgroundColor = UIColor.MyTheme.darkPurple
        layer.cornerRadius = 8
        
        addSubview(breakLine)
        breakLine.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Indent.superSmallOffset())
            make.height.equalTo(0.8)
        }
        
        addSubview(counterLabel)
        counterLabel.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(Indent.superSmallOffset())
            make.leading.greaterThanOrEqualTo(Indent.superSmallOffset())
            make.bottom.equalTo(breakLine.snp.top).offset(-Indent.superSmallOffset())
        }
        
        addSubview(inputTextView)
        inputTextView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview().inset(Indent.superSmallOffset())
            make.bottom.equalTo(counterLabel.snp.top).offset(-Indent.superSmallOffset())
        }
        
        addSubview(outputTextView)
        outputTextView.snp.makeConstraints { (make) in
            make.top.equalTo(breakLine.snp.bottom).offset(Indent.superSmallOffset())
            make.leading.trailing.bottom.equalToSuperview().inset(Indent.superSmallOffset())
        }
    }
}

extension RootConverterView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        counterLabel.text = "\(inputTextView.text.count)/200"
        if inputTextView.text.count >= maxCharacter {
            counterLabel.textColor = .red
            Animations.requireUserAtencion(on: counterLabel)
        } else {
            counterLabel.textColor = .lightGray
        }
        return inputTextView.text.count + (text.count - range.length) <= maxCharacter
    }
}
