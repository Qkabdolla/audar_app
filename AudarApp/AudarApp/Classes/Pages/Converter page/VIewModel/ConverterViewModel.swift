//
//  ConverterViewModal.swift
//  AudarApp
//
//  Created by Мадияр on 4/23/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

class ConverterViewModel {
    
    let networkManager = NetworkManager()
    weak var delegate: UpdateConverterPageDelegate?
    
    var convertedText = ""
    var selectedType: AlphabetType = .cyrillic
    
    func getConvertedText(text: String, type: AlphabetType) {
        
        networkManager.convertText(text: text, type: type, success: { [weak self] (str) in
            self?.convertedText = str
            self?.delegate?.setData()
        }) { (error) in
            self.delegate?.showAlert()
        }
    }
}
