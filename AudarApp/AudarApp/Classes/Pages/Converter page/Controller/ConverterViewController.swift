//
//  ConverterViewController.swift
//  AudarApp
//
//  Created by Мадияр on 3/24/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class ConverterViewController: UIViewController {
    
    // MARK: - Views
    
    let rootView = RootConverterView()
    let viewModel = ConverterViewModel()
    
    lazy private var segmentedController: UISegmentedControl = {
        let segmentedController = UISegmentedControl(items: ["Cyr", "Lat"])
        segmentedController.selectedSegmentIndex = 0
        segmentedController.addTarget(self, action: #selector(didSegmentChanged(_:)), for: .valueChanged)
        return segmentedController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        viewModel.delegate = self
        
        configureNavigationBar()
        layoutUI()
        
        rootView.didDoneTapped = {
            self.viewModel.getConvertedText(text: self.rootView.inputText, type: self.viewModel.selectedType)
        }
    }
    
    private func configureNavigationBar() {
        navigationItem.title = "Converter"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(didShareButtonPressed(_:)))
        navigationItem.leftBarButtonItem?.isEnabled = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "fav_star"), style: .plain, target: self, action: #selector(didSaveButtonPressed(_:)))
    }
    
    @objc private func didShareButtonPressed(_ sender: UIBarButtonItem) {
        let items = [rootView.outputText]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    
    @objc private func didSegmentChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: viewModel.selectedType = .cyrillic
        case 1: viewModel.selectedType = .latin
        default:
            break
        }
        rootView.inputText = rootView.outputText
        viewModel.getConvertedText(text: rootView.inputText, type: viewModel.selectedType)
    }
    
    @objc private func didSaveButtonPressed(_ sender: UIBarButtonItem) {
        let input = self.rootView.inputText
        let output = self.rootView.outputText
        if input == "" {
            Alert.showFillMandatoryFields(vc: self)
        } else {
            let entryModel = EntryModel(input: input, output: output)
            RealmService.shared.create(entryModel)
            Alert.successSaveAlert(vc: self)
        }
    }
    
    // MARK: - Layout UI & Configure
    private func layoutUI() {
        view.addSubview(segmentedController)
        segmentedController.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(Indent.superPuperSmallOffset())
            make.leading.trailing.equalToSuperview().inset(Indent.hugeOffset())
            make.height.equalTo(30)
        }
        
        view.addSubview(rootView)
        rootView.snp.makeConstraints { (make) in
            make.top.equalTo(segmentedController.snp.bottom).offset(Indent.superPuperSmallOffset())
            make.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide).inset(Indent.superPuperSmallOffset())
        }
    }
}

extension ConverterViewController: UpdateConverterPageDelegate {
    func setData() {
        rootView.outputText = viewModel.convertedText
        if rootView.outputText == "" {
            navigationItem.leftBarButtonItem?.isEnabled = false
        } else {
            navigationItem.leftBarButtonItem?.isEnabled = true
        }
    }
    
    func showAlert() {
        Alert.otherError(vc: self)
    }
}
