//
//  FAQViewModel.swift
//  AudarApp
//
//  Created by Мадияр on 4/25/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

struct ExpandableData {
    var isExpanded: Bool = false
    var sectionName: String
    var answerText: String
}

class FAQViewModel {
    
    var data = [
            ExpandableData(sectionName: "Вопрос номер 1?",
                           answerText: "Зарегистрировано еще 40 новых случаев заражения коронавирусной инфекцией, из них в городе Нур-Султан - 8, Западно-Казахстанской области - 7, Павлодарской области - 10, Атырауской области - 4, Кызылординской области - 3, Восточно-Казахстанской области - 1, Туркестанской области - 4, городе Шымкент - 2, Алматинской области - 1"),
            ExpandableData(sectionName: "Вопрос номер 2?",
                           answerText: "Зарегистрировано еще 40 новых случаев заражения коронавирусной инфекцией, из них в городе Нур-Султан - 8, Западно-Казахстанской области - 7,"),
            ExpandableData(sectionName: "Вопрос номер 3?",
                           answerText: "Зарегистрировано еще 40 новых случаев заражения")
        ]
    
    func numberOfElements() -> Int {
        return data.count
    }
    
    func getElement(with section: Int) -> ExpandableData {
        return data[section]
    }
    
    func setData(with section: Int, isExpanded: Bool) {
        data[section].isExpanded = isExpanded
    }
    
}
