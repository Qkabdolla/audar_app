//
//  ProfileViewModel.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/4/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import Foundation

class ProfileViewModel {
    let data: [String] = ["Help", "Tell a Friend", "Contact with us", "About us"]
    let logos: [String] = ["help", "heart", "meeting", "about"]
    
    func numberOfElements() -> Int {
        return data.count
    }
    
    func getElement(with row: Int) -> String {
        return data[row]
    }
    
    func getLogo(with row: Int) -> String {
        return logos[row]
    }
}
