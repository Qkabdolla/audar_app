//
//  ExpandableSectionView.swift
//  AudarApp
//
//  Created by Мадияр on 4/25/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class ExpandableSectionView: UIView {
    
    var titleText = "" {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Medium", size: FontSize.small())
        label.numberOfLines = 0
        return label
    }()
    
    lazy private var expandImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "next")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        return imageView
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor.MyTheme.darkPurple
        
        layoutUI()
    }
    
    private func layoutUI() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(Indent.offset())
        }
        
        addSubview(expandImageView)
        expandImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-Indent.offset())
            make.size.equalTo(10)
        }
        
        addSubview(button)
        button.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
