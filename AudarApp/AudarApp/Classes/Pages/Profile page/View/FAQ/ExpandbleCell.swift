//
//  ExpandbleCell.swift
//  AudarApp
//
//  Created by Мадияр on 4/25/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class ExpandbleCell: UITableViewCell {
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.MyTheme.lightPurple
        isUserInteractionEnabled = false
        
        layoutUI()
    }
    
    private func layoutUI() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(Indent.offset())
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(text: String) {
        titleLabel.text = text
    }
    
}
