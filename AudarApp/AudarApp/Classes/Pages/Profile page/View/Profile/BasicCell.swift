//
//  BasicCell.swift
//  AudarApp
//
//  Created by Мадияр on 4/24/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class BasicCell: UITableViewCell {
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        return label
    }()
    
    lazy private var view: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.MyTheme.darkPurple
        return view
    }()
    
    lazy private var nextView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "next")?.withRenderingMode(.alwaysTemplate)
        view.tintColor = .lightGray
        return view
    }()
    
    lazy private var logoView: UIImageView = {
        let view = UIImageView()
        view.tintColor = .lightGray
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        addSubview(view)
        view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(logoView)
        logoView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(Indent.offset())
            make.size.equalTo(25)
        }
        
        view.addSubview(nextView)
        nextView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-Indent.offset())
            make.size.equalTo(10)
        }
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(logoView.snp.trailing).offset(Indent.offset())
            make.top.bottom.equalToSuperview().inset(Indent.offset())
            make.trailing.lessThanOrEqualTo(nextView.snp.leading).offset(-Indent.offset())
        }
        
    }
    
    func configureCell(text: String, imageName: String) {
        titleLabel.text = text
        logoView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
    }
}
