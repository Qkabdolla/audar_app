//
//  ImageCell.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/4/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit
import SnapKit

class ImageCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.MyTheme.lightPurple
        isUserInteractionEnabled = false
        updateConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(name: String, email: String) {
        categoryLabel.text = name
        emailLabel.text = email
    }
    
    private func updateConstraint() {
        addSubview(userImage)
        userImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Indent.offset())
            make.centerX.equalToSuperview()
            make.size.equalTo(Screen.height() * 0.25)
        }
        
        addSubview(categoryLabel)
        categoryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(userImage.snp.bottom).offset(Indent.offset())
            make.centerX.equalToSuperview()
        }
        
        addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(categoryLabel.snp.bottom).offset(Indent.superSmallOffset())
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-Indent.hugeOffset())
        }
        
    }
    
    lazy private var userImage : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        img.backgroundColor = UIColor.MyTheme.darkPurple
        img.layer.cornerRadius = Screen.height() * 0.25 / 2
        img.clipsToBounds = true
        return img
    }()
    
    lazy private var categoryLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        lbl.text = ""
        return lbl
    }()
    
    lazy private var emailLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Roboto-Regular", size: FontSize.small())
        lbl.text = ""
        return lbl
    }()
    
}
