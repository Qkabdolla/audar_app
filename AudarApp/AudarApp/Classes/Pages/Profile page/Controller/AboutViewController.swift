//
//  AboutViewController.swift
//  AudarApp
//
//  Created by Мадияр on 4/25/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    lazy private var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .lightGray
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.medium())
        label.text = "© 2020 Audar Team All Rights Reserved"
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        navigationItem.title = "About Us"
        
        layoutUI()
    }

    private func layoutUI() {
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
}
