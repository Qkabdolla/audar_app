//
//  ProfileViewController.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/4/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit
import SnapKit

class ProfileViewController: UIViewController {
    
    var tableView = UITableView()
    
    let viewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableview()
        updateConstraint()
        setUpNavigation()
    }
    
    private func setTableview() {
        
        tableView.backgroundColor = UIColor.MyTheme.lightPurple
            
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self

        tableView.register(ImageCell.self, forCellReuseIdentifier: "imageCell")
        tableView.register(BasicCell.self, forCellReuseIdentifier: "basicCell")
    }
    
    private func updateConstraint() {
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
          if #available(iOS 11, *) {
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
          } else {
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
          }
        }
    }
    
    private func setUpNavigation() {
        navigationItem.title = "Profile"

        let leftItem = UIBarButtonItem(image: UIImage(named: "out"), style: .done, target: self, action: #selector(handleLogOut(_:)))
        navigationItem.rightBarButtonItem = leftItem
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc private func handleLogOut(_ sender: UIBarButtonItem) {
        userDefaults.setLoggedIn(value: false)
        Switcher.updateRootVC()
    }
    
    private func showActivityVC() {
        let text = "Audar is best app which I ve ever used!"

        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]

        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfElements() + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as! ImageCell
            let name = userDefaults.getName()
            let email = userDefaults.getEmail()
            
            cell.configureCell(name: name, email: email)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath) as! BasicCell
            let text = viewModel.getElement(with: indexPath.row - 1)
            let imageName = viewModel.getLogo(with: indexPath.row - 1)
            
            cell.configureCell(text: text, imageName: imageName)
            return cell
        }
        
    }
}

extension ProfileViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 1:
            navigationController?.pushViewController(FAQViewController(), animated: true)
        case 2:
            showActivityVC()
        case 3:
            Alert.showActionSheet(on: self, firstHandler: { (action) in
                
            }, secondHandler: { (action) in
                
            }, thirdHandler: { (action) in
                
            })
        case 4:
            navigationController?.pushViewController(AboutViewController(), animated: true)
        default:
            break
        }
    }

}
