//
//  FAQViewController.swift
//  AudarApp
//
//  Created by Мадияр on 4/25/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {
    
    private let viewModel = FAQViewModel()
    
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        view.dataSource = self
        view.delegate = self
        view.register(ExpandbleCell.self, forCellReuseIdentifier: "cell")
        view.tableHeaderView = UIView()
        view.showsVerticalScrollIndicator = false
        view.separatorStyle = .none
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        navigationItem.title = "Help"
        
        layoutUI()
    }
    
    private func layoutUI() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(Indent.superSmallOffset())
            make.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }
}

extension FAQViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfElements()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !viewModel.getElement(with: section).isExpanded {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExpandbleCell
        let item = viewModel.getElement(with: indexPath.section).answerText
        cell.configureCell(text: item)
        return cell
    }
}

extension FAQViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = ExpandableSectionView()
        view.titleText = viewModel.getElement(with: section).sectionName
        view.button.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        view.button.tag = section
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func handleExpandClose(button : UIButton) {
        let section = button.tag
        let indexPaths: [IndexPath] = [IndexPath(row: 0, section: section)]

        let isExpanded = viewModel.getElement(with: section).isExpanded
        viewModel.setData(with: section, isExpanded: !isExpanded)

        if isExpanded {
            tableView.deleteRows(at: indexPaths, with: .top)
        }
        else {
            tableView.insertRows(at: indexPaths, with: .top)
        }
    }
    
}
