//
//  ButtonCollectionCell.swift
//  AudarApp
//
//  Created by Мадияр on 4/15/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class ButtonCollectionCell: UICollectionViewCell {
    
    lazy private var logoView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: FontSize.medium())
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    lazy private var bodyStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [logoView, titleLabel])
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.spacing = 0
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        layer.cornerRadius = 20
        backgroundColor = .clear
    
        setupBlur()
        
        addSubview(bodyStackView)
        bodyStackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    private func setupBlur() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)

        blurView.layer.cornerRadius = 30
        blurView.clipsToBounds = true
        blurView.alpha = 0.85
        insertSubview(blurView, at: 0)
        
        blurView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func configureCell(text: String, image: String) {
        titleLabel.text = text
        logoView.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
        logoView.tintColor = .white
    }
    
}
