//
//  HomeViewModel.swift
//  AudarApp
//
//  Created by Мадияр on 4/16/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import Foundation

struct Task {
    let title: String
    let image: String
}

class HomeViewModel {
    let data: [Task] = [Task(title: "alphabet", image: "alphabet"),
                        Task(title: "saved", image: "star"),
                        Task(title: "converter", image: "convert"),
                        Task(title: "keyboard", image: "keyboard")
    ]
    
    func numberOfData() -> Int {
        return data.count
    }
    
    func getElement(with row: Int) -> Task {
        return data[row]
    }
}
