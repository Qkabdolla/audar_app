//
//  HomeViewController.swift
//  AudarApp
//
//  Created by Мадияр on 3/24/20.
//  Copyright © 2020 Мадияр. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Views
    
    let bgImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.image = UIImage(named: "main-BG")
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Audar Team"
        label.textColor = .white
        label.font = UIFont(name: "Montserrat-Bold", size: FontSize.superHuge())
        return label
    }()
    
    lazy private var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ButtonCollectionCell.self, forCellWithReuseIdentifier: "Cells")
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    let viewModel = HomeViewModel()
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.MyTheme.lightPurple
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        layoutUI()
    }
    
    // MARK: - Layout UI & Configure
    private func layoutUI() {
        view.addSubview(bgImage)
        bgImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        bgImage.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(Indent.offset())
            make.top.equalToSuperview().offset(view.bounds.height * 0.1)
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cells", for: indexPath) as! ButtonCollectionCell
        
        let item = viewModel.getElement(with: indexPath.row)
        cell.configureCell(text: item.title, image: item.image)
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: self.navigationController?.pushViewController(AlphabetController(), animated: true)
        case 1: self.navigationController?.pushViewController(SavedController(), animated: true)
        case 2: Switcher.setConverter()
        case 3: self.present(KeyboardViewController(), animated: true)
        default:
            break
        }
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width * 0.43, height: view.bounds.width * 0.43)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: view.bounds.height * 0.2, left: 16, bottom: 0, right: 16)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Indent.offset()
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Indent.offset()
    }
}
