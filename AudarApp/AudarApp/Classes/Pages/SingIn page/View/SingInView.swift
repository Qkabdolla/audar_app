//
//  SignInView.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/3/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit
import SnapKit

class SignInView : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateConstraint()
        
        emailtextFields.delegate = self
        passwordtextFields1.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing(true)
    }
    
     func updateConstraint() {
        addSubview(backGroundViews)
        backGroundViews.snp.makeConstraints { make in
            make.edges.equalToSuperview()
       }
        
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
          if #available(iOS 11, *) {
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).offset(90)
            make.centerX.equalToSuperview()
            make.size.equalTo(150)
          } else {
            make.top.equalTo(70)
            make.centerX.equalToSuperview()
            make.size.equalTo(150)
          }
        }
        
        addSubview(emailtextFields)
        emailtextFields.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(30)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(passwordtextFields1)
        passwordtextFields1.snp.makeConstraints { (make) in
            make.top.equalTo(emailtextFields.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(signInButton)
        signInButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordtextFields1.snp.bottom).offset(15)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(orLabel)
        orLabel.snp.makeConstraints { (make) in
            make.top.equalTo(signInButton.snp.bottom).offset(16)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(showButton)
        showButton.snp.makeConstraints { (make) in
            make.top.equalTo(orLabel.snp.bottom).offset(16)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(40)
        }
        
        addSubview(activityView)
        activityView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(110)
            make.centerX.equalTo(imageView.snp.centerX)
        }
    }
    
    var backGroundViews : UIView = {
        let  views = UIView()
        views.backgroundColor = UIColor.MyTheme.darkPurple
        return views
    }()
    
    var imageView : UIImageView = {
        let icons = UIImageView()
        icons.backgroundColor = .gray
        icons.image = UIImage(named: "appstore")
        return icons
    }()
    
    var emailtextFields : UITextField = {
        let tf = UITextField()
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor.MyTheme.darkPurple
        tf.textColor = .white
        tf.placeholder = "Username"
        tf.placeholderColor(.white)
        tf.autocapitalizationType = .none
        tf.autocorrectionType = .no
        tf.returnKeyType = .done
        return tf
    }()
    
    var passwordtextFields1 : UITextField = {
        let password = UITextField()
        password.backgroundColor = UIColor.MyTheme.darkPurple
        password.textColor = .white
        password.borderStyle = .roundedRect
        password.placeholder = "Password"
        password.placeholderColor(.white)
        password.autocapitalizationType = .none
        password.autocorrectionType = .no
        password.isSecureTextEntry = true
        password.returnKeyType = .done
        return password
    }()
    
    var signInButton : UIButton = {
        let button = UIButton(type: .system) // let preferred over var here
        button.backgroundColor =  UIColor(red: 233.0/255, green: 63.0/255, blue: 137.0/255, alpha: 1.0)
        button.layer.cornerRadius  = 5
        button.tintColor = .white
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        button.setTitle("LOGIN", for: UIControl.State.normal)
        return button
    }()
    
    var orLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir Next", size: 17)
        lbl.text = "or"
        return lbl
    }()
    
    var showButton : UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .lightGray
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 16)
        button.setTitle("Register Now", for: UIControl.State.normal)
        return button
    }()
    
    var activityView : UIActivityIndicatorView = {
        let av = UIActivityIndicatorView()
        av.style = .large
        av.color = .magenta
        return av
    }()
   
}

extension SignInView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
}
