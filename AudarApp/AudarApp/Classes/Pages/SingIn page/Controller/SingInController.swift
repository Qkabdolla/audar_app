//
//  SingInViewController.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 2/27/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit

class SingInViewController: UIViewController {
      
    let networkManager = NetworkManager()
    
    var rootView: SignInView { return self.view as! SignInView}
       
    override func loadView() {
        self.view = SignInView()
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        configurView()
    }

    private func configurView() {
        rootView.showButton.addTarget(self, action: #selector(pushController), for: .touchUpInside)
        rootView.signInButton.addTarget(self, action: #selector(dataTransfer(_:)), for: .touchUpInside)
    }
       
    @objc func pushController() {
        let newViewController = FirstViewController()
        newViewController.modalPresentationStyle = .fullScreen
        self.present(newViewController, animated: true, completion: nil)
    }
       
    func presentController() {
        let vc = CustomTabBarController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
       
    @objc func dataTransfer(_ sender: UIButton) {
           
        rootView.activityView.startAnimating()
           
        guard let username = rootView.emailtextFields.text, let password = rootView.passwordtextFields1.text else { return }
        
        if username == "" || password == "" {
            rootView.activityView.stopAnimating()
            Alert.showFillMandatoryFields(vc: self)
            return
        }
              
        networkManager.signin(username: username, password: password, success: {
            self.presentController()
            self.rootView.activityView.stopAnimating()
            Switcher.updateRootVC()
        }) { (error) in
            self.rootView.activityView.stopAnimating()
            switch error {
            case .incorrectData: Alert.incorrectData(vc: self)
            case .other: Alert.otherError(vc: self)
            }
        }
        
    }
}
