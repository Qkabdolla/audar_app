//
//  AppDelegate.swift
//  TestApp
//
//  Created by Koltubaev Aslan on 1/9/20.
//  Copyright © 2020 Koltubaev Aslan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let realiviingSplashView = LaunchScreenSplashView(iconImage: UIImage(named: "appstore")!, iconInitialSize: CGSize(width: 414, height: 414), backgroundColor: UIColor.MyTheme.darkPurple)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  
        window = UIWindow(frame: UIScreen.main.bounds)
        Switcher.updateRootVC()
        window?.makeKeyAndVisible()
        
        window?.addSubview(realiviingSplashView)
        realiviingSplashView.startAnimation()
        
        configureNavigationBar()
        return true
    }
    
    private func configureNavigationBar() {
        let appearance = UINavigationBar.appearance()
        appearance.backgroundColor = UIColor(red: 39.0/255, green: 30.0/255, blue: 76.0/255, alpha: 1.0)
        appearance.tintColor = UIColor.white
        appearance.isTranslucent = false
        appearance.barTintColor = UIColor(red: 39.0/255, green: 30.0/255, blue: 76.0/255, alpha: 1.0)
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.prefersLargeTitles = false
    }

}

